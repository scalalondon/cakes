package uk.co.techneurons

/**
 * Using self-type annotation declaring the dependencies this component requires, in our case, the
 * UserRepositoryComponent.
 */
trait UserServiceComponent { this: UserRepositoryComponent =>

  val userService: UserService

  class UserService {

    def create(email: String, password: String) = userRepository.create(User(None, email, password))

    def delete(user: User) = userRepository.delete(user)

  }


}
