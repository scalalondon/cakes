package uk.co.techneurons

/**
 * User case class
 */
case class User(id: Option[Long], email: String, password: String)