package uk.co.techneurons

/**
 * In order to wire the application, the only thing we need to do is to merge/join the different
 * namespaces into one single application (or module) namespace. This is done by creating a “module”
 * object composed of all our components. When we do that all wiring is happening automatically
 */
object ComponentRegistry extends  UserServiceComponent with UserRepositoryComponent {

  val userRepository = new UserRepository
  val userService = new UserService

}
