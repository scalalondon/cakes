package uk.co.techneurons


import org.scalatest.mock.EasyMockSugar

/**
 * Test Environment.
 */
trait TestContext extends UserServiceComponent with UserRepositoryComponent with EasyMockSugar {

  val userService = mock[UserService]
  val userRepository = mock[UserRepository]

}
