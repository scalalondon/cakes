package uk.co.techneurons

import org.scalatest.{FlatSpec, Matchers}

import org.easymock.EasyMock._

/**
 * Testing a Service in Scala.
 */
class UserServiceSpec extends FlatSpec with TestContext with Matchers {

  override val userService = new UserService

  "UserService" should "allow creation of a user" in {

    val email = "xxx@xxx.com"
    val password = "abc123"

    expecting {
      userRepository.create(User(None, email, password))
    }

    replay(userRepository)
    userService.create("xxx@xxx.com","abc123")
    verify(userRepository)
  }

}
