The Cake Pattern for Dependency Injection
=========================================
TDD/BDD developers depend on Dependency Injection. How does Scala fit into this picture? Well you don't
need a DI framework. The Cake Pattern can be used for compile time DI. But it is possible to also use Spring or Guice
if required.

The Play Framework Supports Guice out of the box now as of version 2.4
More detail [Play 2.4 Migration Guide](https://www.playframework.com/documentation/2.4.x/Migration24 "Dependency Injection").

Cake Pattern
------------
I won't explain the process - I will leave that to the real experts - see
[JonasBoner](http://jonasboner.com/2008/10/06/real-world-scala-dependency-injection-di/)

I have coded an complete implementation for reference

Testing
-------
[EasyMock and ScalaTest](http://www.scalatest.org/user_guide/testing_with_mock_objects)